# README #

Contained within are some examples of projects I have done that contribute a fuller understanding of my accomplishments in engineering.

### Included Projects ###

* A Generative Adversarial Neural Net implemented in TensorFlow
  used to increase the resolution of input images
  that attempts to implement a paper achieving the same target (2018).
* Say Hello! A conversational AI for use with Alexa via the Alexa Skill API targeted
  at helping ESL students learn English (2018).
* A linux distro using a custom kernel version provided by Xilinx and a custom root
  filesystem targeting an embedded PowerPC on a Virtex V FPGA. Bootloaders were also
  implemented for this project (2018).
* A multi layer perceptron neural net accelerator laid out via SAPR with a coprocessor
  in custom layout used to load weights and data into the MLP.
* SayHello, a conversational AI for Alexa targeting ESL Learners.
* Super Resolution, an implementation of a paper released in early 2018 of the same name,
  which implements photorealistic super resolution of images.
* Puppybot, a autonomous tank robot that tracks and follows movements.

Feel free to contact me with questions at dustinwerran@gmail.com.
